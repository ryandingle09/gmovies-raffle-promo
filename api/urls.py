from django.urls import path
from . import views

app_name = 'api'
urlpatterns = [
    # update to cancel transaction entry
    path('v1/transaction/cancel/<str:transaction_id>', views.Transaction.cancel, name='api-cancel_transation'),
    path('v1/transaction/void/<str:transaction_id>', views.Transaction.void_transaction, name='api-void_transation'),
    path('v1/transaction/unvoid/<str:transaction_id>', views.Transaction.unvoid_transaction, name='api-unvoid_transation'),
    path('v1/transaction/report/<str:report_filter>', views.Transaction.create_report, name='api-create_report'),
]