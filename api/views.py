from django.http import JsonResponse
from .models import *
from raffle.models import Transaction as Transactions
from django.core.cache import cache
from django.http import HttpResponse

import datetime
import requests
import json
import ast
import xlsxwriter
import io

class Transaction:
    def cancel(self, transaction_id):
        message     = 'Transaction successfully tagged as TX_REFUNDED'
        status      = 1
        transaction = Transactions.objects.filter(gae_transaction_id=transaction_id)

        if not transaction.count():
            message = 'Transaction not found.'
            status  = 0

        else:
            transaction.update(state='TX_REFUNDED', updated_at=datetime.datetime.now())

            # call void entries on raffle engine
            # req = requests.get()

            
        return JsonResponse({'status': status, 'tx_id' : transaction_id, 'message' : message})

    def void_transaction(self, transaction_id):
        status          = 1
        transaction     = Transactions.objects.filter(gae_transaction_id=transaction_id)
        previous_state  = transaction[0].state
        message         = 'Transaction successfully tagged as TX_VOIDED from %s' % previous_state

        if not transaction.count():
            message = 'Transaction not found.'
            status  = 0

        else:
            transaction.update(state='TX_VOIDED', previous_state=previous_state, updated_at=datetime.datetime.now())

            r_transaction   = json.loads(str(cache.get('Transactions')).replace('"{', '{').replace('}"', '}').replace('"[', '[').replace(']"', ']').replace("'", '"').replace('None', 'null'))
            s_transaction   = r_transaction[transaction_id]
            s_transaction['data']['primary_info'][0]['state'] = 'TX_VOIDED'
            s_transaction['state'] = 'TX_VOIDED'

            r_transaction[transaction_id] = s_transaction

            cache.set('Transactions', r_transaction, timeout=None)

            # call void entries on raffle engine
            # req = requests.get()
            
        return JsonResponse({'status': status, 'tx_id' : transaction_id, 'message' : message}, safe=False)

    def unvoid_transaction(self, transaction_id):
        status          = 1
        transaction     = Transactions.objects.filter(gae_transaction_id=transaction_id)
        previous_state  = transaction[0].previous_state
        message         = 'Transaction successfully tagged as %s from TX_VOIDED' % previous_state

        if not transaction.count():
            message = 'Transaction not found.'
            status  = 0

        else:
            transaction.update(state=previous_state, updated_at=datetime.datetime.now())

            r_transaction   = json.loads(str(cache.get('Transactions')).replace('"{', '{').replace('}"', '}').replace('"[', '[').replace(']"', ']').replace("'", '"').replace('None', 'null'))
            s_transaction   = r_transaction[transaction_id]
            s_transaction['data']['primary_info'][0]['state'] = previous_state
            s_transaction['state'] = previous_state

            r_transaction[transaction_id] = s_transaction

            cache.set('Transactions', r_transaction, timeout=None)

            # call unvoid entries on raffle engine
            # req = requests.get()
            
        return JsonResponse({'status': status, 'tx_id' : transaction_id, 'message' : message})
    
    def create_report(self, report_filter):
        search, org, start, end = report_filter.split('~')
        filename = "RafflePromoTransactionReport.xlsx"

        tr = Transactions.objects.all()
        if search:
            tr = tr.filter(theater_name__contains=search) | \
                 tr.filter(theaterorg_name__contains=search) | \
                 tr.filter(transaction_date__contains=search) | \
                 tr.filter(show_date__contains=search) | \
                 tr.filter(ticket_code__contains=search) | \
                 tr.filter(payment_type__contains=search) | \
                 tr.filter(state__contains=search) | \
                 tr.filter(platform__contains=search) | \
                 tr.filter(payment_reference_code__contains=search) | \
                 tr.filter(reservation_reference_code__contains=search) | \
                 tr.filter(transaction_entry__contains=search) | \
                 tr.filter(multiplier__contains=search) | \
                 tr.filter(customer__first_name__contains=search) | \
                 tr.filter(customer__last_name__contains=search) | \
                 tr.filter(customer__email__contains=search) | \
                 tr.filter(customer__mobile__contains=search)
        if org:
            tr = tr.filter(theaterorg_name__contains=org.lower())
        if start != '' and end != '':
            tr = tr.filter(created_at__range=(start_date, end_date))
        
        row_counter = 0

        output = io.BytesIO()

        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        worksheet = workbook.add_worksheet()
        headers_font_style = workbook.add_format({'bold': True})

        worksheet.write(0, 0, 'TICKET CODE', headers_font_style)
        worksheet.write(0, 1, 'DATE & TIME', headers_font_style)
        worksheet.write(0, 2, 'CUSTOMER NAME', headers_font_style)
        worksheet.write(0, 3, 'THEATER', headers_font_style)
        worksheet.write(0, 4, 'CINEMA', headers_font_style)
        worksheet.write(0, 5, 'SEATS', headers_font_style)
        worksheet.write(0, 6, 'PLATFORM', headers_font_style)
        worksheet.write(0, 7, 'PAYMENT METHOD', headers_font_style)
        worksheet.write(0, 8, 'EMAIL', headers_font_style)
        worksheet.write(0, 9, 'MOBILE', headers_font_style)
        worksheet.write(0, 10, 'POSTAL ADDRESS', headers_font_style)
        worksheet.write(0, 11, 'ADDITIONAL ENTRIES', headers_font_style)
        worksheet.write(0, 12, 'TRANSACTION EARNINGS', headers_font_style)
        worksheet.write(0, 13, 'STATUS', headers_font_style)

        for transaction in tr:
            row_counter += 1
            name = ""
            status = "Valid"
            fname = transaction.customer.first_name
            lname = transaction.customer.last_name
            additional_entries = ""
            for multiplier in ast.literal_eval(transaction.multiplier):
                if multiplier == 'AA':
                    if additional_entries:
                        additional_entries = ", ".join([additional_entries, "Advance Booking"])
                    else:
                        additional_entries = "Advance Booking"
                elif multiplier == 'B':
                    if additional_entries:
                        additional_entries = ", ".join([additional_entries, "Globe User"])
                    else:
                        additional_entries = "Globe User"
                elif multiplier == 'c':
                    if additional_entries:
                        additional_entries = ", ".join([additional_entries, "GCash App"])
                    else:
                        additional_entries = "GCash App"
                elif multiplier == 'D':
                    if additional_entries:
                        additional_entries = ", ".join([additional_entries, "GCash Payment"])
                    else:
                        additional_entries = "GCash Payment"
            
            if fname or lname:
                name = ' '.join([fname, lname])

            if transaction.state != 'TX_DONE':
                status = "Invalid"
            
            worksheet.write(row_counter, 0, transaction.ticket_code)
            worksheet.write(row_counter, 1, transaction.transaction_date.strftime("%d %B %Y ; %H:%M:%S %p"))
            worksheet.write(row_counter, 2, name)
            worksheet.write(row_counter, 3, transaction.theaterorg_name)
            worksheet.write(row_counter, 4, transaction.theater_name)
            worksheet.write(row_counter, 5, transaction.seat_ids)
            worksheet.write(row_counter, 6, transaction.platform)
            worksheet.write(row_counter, 7, transaction.payment_type)
            worksheet.write(row_counter, 8, transaction.customer.email)
            worksheet.write(row_counter, 9, transaction.customer.mobile)
            worksheet.write(row_counter, 10, transaction.customer.postal_addr)
            worksheet.write(row_counter, 11, additional_entries)
            worksheet.write(row_counter, 12, transaction.transaction_entry)
            worksheet.write(row_counter, 13, status)
        
        workbook.close()
        output.seek(0)

        response = HttpResponse(output.read(),
            content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        )
        response['Content-Disposition'] = "attachment; filename=%s" % filename #".".join([filename, 'xlsx'])
        output.close()
        return response