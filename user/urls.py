from django.urls import path
from . import views

app_name = 'user'
urlpatterns = [
    path('list', views.List.as_view(), name='user-list'),
    path('add', views.Add.as_view(), name='add'),
    path('edit/<int:user_id>', views.Edit.as_view(), name='edit'),
    path('delete/<int:user_id>', views.Delete.as_view(), name='delete'),
    path('update-profile/<int:user_id>', views.UpdateProfile.as_view(), name='updateprofile'),
    path('change-password/<int:user_id>', views.ChangePassword.as_view(), name='changepassword'),
    path('change-password-from-profile/<int:user_id>', views.ChangePassword2.as_view(), name='changepassword2'),
    path('profile', views.Profile.as_view(), name='profile'),
]