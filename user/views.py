from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.views import generic, View
from django.utils import timezone
from django.contrib.auth.models import User
from group.models import UL as Group
from .models import UserInfo
from group.models import User as UserGroups
from validate_email import validate_email

class List(LoginRequiredMixin,View):
    template_name   = 'user/list.html'

    def get(self, request, *args, **kwargs):
        offset      = int(request.GET['offset']) if 'offset' in request.GET else 0
        limit       = int(request.GET['limit']) if 'limit' in request.GET else 10
        page        = int(request.GET['page']) if 'page' in request.GET else 1
        search      = str(request.GET['search']) if 'search' in request.GET else None

        items       = User.objects.filter(is_superuser=False)
        paginator   = Paginator(items, limit)
        data_list   = paginator.get_page(page)

        data = {
            'items': data_list,
            'all_count': paginator.count,
            'current_count': len(data_list),
            'offset': offset,
            'limit': limit,
            'page': page,
        }

        if search is not None:
            items = User.objects.filter(is_superuser=False)

            if search != '':
                items = User.objects.filter(first_name__contains=search) | User.objects.filter(last_name__contains=search) | User.objects.filter(email__contains=search) | User.objects.filter(userinfo__mobile__contains=search)

            paginator   = Paginator(items, limit)
            data_list   = paginator.get_page(page)

            data = {
                'items': data_list,
                'all_count': paginator.count,
                'current_count': len(data_list),
                'offset': offset,
                'limit': limit,
                'page': page,
            }

        return render(request, self.template_name, data)

class Add(LoginRequiredMixin,View):
    errors          = []
    template_name   = 'user/add.html'
    message         = ''

    def get(self, request):
        data = {
            'groups': Group.objects.all(),
            'message': self.message,
            'errors': self.errors
        }

        return render(request, self.template_name, data)
    
    def post(self, request):
        u_first_name    = request.POST['first_name'] if request.POST['first_name'] != '' else None
        u_last_name     = request.POST['last_name'] if request.POST['last_name'] != '' else None
        u_email         = request.POST['email'] if request.POST['email'] != '' else None
        u_mobile        = request.POST['mobile'] if request.POST['mobile'] != '' else None
        u_group         = request.POST['group'] if request.POST['group'] != '' else None
        u_is_active     = True if 'is_active' in request.POST and request.POST['is_active'] == '1' else False

        self.errors = []

        if u_first_name is None:
            self.errors.append('First Name Field is Required.')
        
        if u_last_name is None:
            self.errors.append('Last Name Field is Required.')

        if u_email is None:
            
            self.errors.append('Email Field is Required.')

        if u_email is not None:
            is_valid = validate_email(u_email)

            if not is_valid:
                self.errors.append('Invalid Email Address.')

            check = User.objects.filter(email=u_email)
            if len(check) != 0 and str(check[0].email) == str(u_email):
                self.errors.append('Email Address Already Taken.')

        if u_mobile is None:
            self.errors.append('Mobile Field is Required.')

        if u_group is None:
            self.errors.append('Group Field is Required.')
        
        if len(self.errors) == 0:
            self.message = 'Successfully Added.'
            u = User(first_name=u_first_name, last_name=u_last_name, username=u_email ,email=u_email, is_active=u_is_active)
            u.save()

            UserInfo(mobile=u_mobile, user_id=u.id).save()
            UserGroups(user_id=u.id, group_id=int(u_group)).save()

            data = {
                'message': self.message,
                'errors': self.errors
            }

            return JsonResponse(data)

        self.message = 'Fields are required.'

        data = {
            'message': self.message,
            'errors': self.errors
        }

        return JsonResponse(data)
    
class Edit(LoginRequiredMixin,View):
    errors          = []
    template_name   = 'user/edit.html'
    message         = ''

    def get(self, request, user_id):
        u = User.objects.filter(id=user_id)
        data = {
            'items': u,
            'groups': Group.objects.all(),
            'message': self.message,
            'errors': self.errors
        }

        return render(request, self.template_name, data)
    
    def post(self, request, user_id):
        u_first_name    = request.POST['first_name'] if request.POST['first_name'] != '' else None
        u_last_name     = request.POST['last_name'] if request.POST['last_name'] != '' else None
        u_email         = request.POST['email'] if request.POST['email'] != '' else None
        u_mobile        = request.POST['mobile'] if request.POST['mobile'] != '' else None
        u_group         = request.POST['group'] if request.POST['group'] != '' else None
        u_is_active     = True if 'is_active' in request.POST and request.POST['is_active'] == '1' else False

        g = User.objects.filter(id=user_id)

        self.errors = []

        if u_first_name is None:
            self.errors.append('First Name Field is Required.')
        
        if u_last_name is None:
            self.errors.append('Last Name Field is Required.')

        if u_email is None:
            self.errors.append('Email Field is Required.')

        if u_email is not None:
            is_valid = validate_email(u_email)

            if not is_valid:
                self.errors.append('Invalid Email Address.')
            
            check = User.objects.filter(email=u_email)
            
            if str(g[0].email) != (u_email):
                if len(check) != 0 and str(check[0].email) == str(u_email):
                    self.errors.append('Email Address Already Taken.')

        if u_mobile is None:
            self.errors.append('Mobile Field is Required.')

        if u_group is None:
            self.errors.append('Group Field is Required.')
        
        if len(self.errors) == 0:
            self.message = 'Successfully Updated.'
            u = User.objects.filter(id=user_id)
            u.update(first_name=u_first_name, last_name=u_last_name, username=u_email ,email=u_email, is_active=u_is_active)

            m = UserInfo.objects.filter(user_id=user_id)
            m.update(mobile=u_mobile)

            ug = UserGroups.objects.filter(user_id=user_id)
            ug.update(group_id=int(u_group))

            data = {
                'message': self.message,
                'errors': self.errors
            }

            return JsonResponse(data)

        self.message = 'Fields are required.'

        data = {
            'message': self.message,
            'errors': self.errors
        }

        return JsonResponse(data)

class ChangePassword(LoginRequiredMixin,View):
    errors          = []
    template_name   = 'user/password.html'
    message         = ''

    def get(self, request, user_id):
        u = User.objects.filter(id=user_id)
        data = {
            'items': u,
            'message': self.message,
            'errors': self.errors
        }

        return render(request, self.template_name, data)

    def post(self, request, user_id):
        password1 = str(request.POST['password']) if request.POST['password'] else None
        password2 = str(request.POST['password2']) if request.POST['password2'] else None

        u = User.objects.get(id=user_id)

        self.errors = []

        if password1 is None:
            self.errors.append('Password Field is required.')

        if password2 is None:
            self.errors.append('Confirm Password Field is required.')

        if password1 is not None or password2 is not None:
            if password1 != password2:
                self.errors.append('Password dont match.')
            
            else:
                u.set_password(password1)
                u.save()

                data = {
                    'message': self.message,
                    'errors': self.errors
                }

                return JsonResponse(data)
        
        self.message = 'Fields are required.'

        data = {
            'message': self.message,
            'errors': self.errors
        }

        return JsonResponse(data)

class UpdateProfile(LoginRequiredMixin, View):
    errors          = []
    message         = ''

    def post(self, request, user_id):
        u_first_name    = request.POST['first_name'] if request.POST['first_name'] != '' else None
        u_last_name     = request.POST['last_name'] if request.POST['last_name'] != '' else None
        u_email         = request.POST['email'] if request.POST['email'] != '' else None
        u_mobile        = request.POST['mobile'] if request.POST['mobile'] != '' else None

        g = User.objects.filter(id=user_id)

        self.errors = []

        if u_first_name is None:
            self.errors.append('First Name Field is Required.')
        
        if u_last_name is None:
            self.errors.append('Last Name Field is Required.')

        if u_email is None:
            self.errors.append('Email Field is Required.')

        if u_email is not None:
            is_valid = validate_email(u_email)

            if not is_valid:
                self.errors.append('Invalid Email Address.')
            
            check = User.objects.filter(email=u_email)
            
            if str(g[0].email) != (u_email):
                if len(check) != 0 and str(check[0].email) == str(u_email):
                    self.errors.append('Email Address Already Taken.')

        if u_mobile is None:
            self.errors.append('Mobile Field is Required.')
        
        if len(self.errors) == 0:
            self.message = 'Successfully Updated.'
            u = User.objects.filter(id=user_id)
            u.update(first_name=u_first_name, last_name=u_last_name, username=u_email ,email=u_email)

            m = UserInfo.objects.filter(user_id=user_id)
            m.update(mobile=u_mobile)

            data = {
                'message': self.message,
                'errors': self.errors
            }

            return JsonResponse(data)

        self.message = 'Fields are required.'

        data = {
            'message': self.message,
            'errors': self.errors
        }

        return JsonResponse(data)

class ChangePassword2(LoginRequiredMixin,View):
    errors          = []
    template_name   = 'user/password2.html'
    message         = ''

    def get(self, request, user_id):
        u = User.objects.filter(id=user_id)
        data = {
            'items': u,
            'message': self.message,
            'errors': self.errors
        }

        return render(request, self.template_name, data)

    def post(self, request, user_id):
        password1 = str(request.POST['password']) if request.POST['password'] else None
        password2 = str(request.POST['password2']) if request.POST['password2'] else None

        u = User.objects.get(id=user_id)

        self.errors = []

        if password1 is None:
            self.errors.append('Password Field is required.')

        if password2 is None:
            self.errors.append('Confirm Password Field is required.')

        if password1 is not None or password2 is not None:
            if password1 != password2:
                self.errors.append('Password dont match.')
            
            else:
                u.set_password(password1)
                u.save()

                data = {
                    'message': self.message,
                    'errors': self.errors
                }

                return JsonResponse(data)
        
        self.message = 'Fields are required.'

        data = {
            'message': self.message,
            'errors': self.errors
        }

        return JsonResponse(data)


class Delete(LoginRequiredMixin,View):
    def get(self, request, user_id):
        User.objects.filter(id=user_id).delete()

        return HttpResponseRedirect('/user/list?status=deleted')

class Profile(LoginRequiredMixin, View):
    errors          = []
    template_name   = 'user/profile.html'
    message         = ''

    def get(self, request):
        u = User.objects.filter(id=request.user.id)
        data = {
            'items': u,
            'message': self.message,
            'errors': self.errors
        }

        return render(request, self.template_name, data)

    def post(self, request):
        first_name  = str(request.POST['first_name']) if request.POST['first_name'] else None
        last_name   = str(request.POST['first_name']) if request.POST['last_name'] else None
        email       = str(request.POST['first_name']) if request.POST['email'] else None
        mobile      = str(request.POST['first_name']) if request.POST['mobile'] else None
        password    = str(request.POST['first_name']) if request.POST['password'] else None

        u = User.objects.get(id=request.user.id)

        self.errors = []

        if first_name is None:
            self.errors.append('Password Field is required.')

        if first_name is None:
            self.errors.append('Confirm Password Field is required.')

        if first_name is None:
            self.errors.append('Confirm Password Field is required.')

        if first_name is None:
            self.errors.append('Confirm Password Field is required.')

        if password1 is not None or password2 is not None:
            if password1 != password2:
                self.errors.append('Password dont match.')
            
            else:
                u.set_password(password1)
                u.save()

                data = {
                    'message': self.message,
                    'errors': self.errors
                }

                return JsonResponse(data)
        
        self.message = 'Fields are required.'

        data = {
            'message': self.message,
            'errors': self.errors
        }

        return JsonResponse(data)
