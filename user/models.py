from django.db import models
from django.contrib.auth.models import User

class UserInfo(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mobile = models.CharField(max_length=30, null=True)
    avatar = models.TextField(null=True)
