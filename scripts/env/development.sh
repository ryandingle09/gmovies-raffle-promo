#####################################
## 
##   _____  ________      ________ _      ____  _____  __  __ ______ _   _ _______ 
##  |  __ \|  ____\ \    / /  ____| |    / __ \|  __ \|  \/  |  ____| \ | |__   __|
##  | |  | | |__   \ \  / /| |__  | |   | |  | | |__) | \  / | |__  |  \| |  | |   
##  | |  | |  __|   \ \/ / |  __| | |   | |  | |  ___/| |\/| |  __| | . ` |  | |   
##  | |__| | |____   \  /  | |____| |___| |__| | |    | |  | | |____| |\  |  | |   
##  |_____/|______|   \/   |______|______\____/|_|    |_|  |_|______|_| \_|  |_|   
##                                                                                 
## 
#####################################
## Prepare your Canary Server here.
#####################################

#############################################
## CODECOMMIT REPOSITORY NAME - *REQUIRED* ##
#############################################
## Used for log and metrics tagging.       ##
#############################################
PROJECT_NAME=CICD-Gmovies-Raffle-API


#############################################################################
## OS / APPLICATION SPECIFIC COMMANDS 
#############################################################################
## Prepare the instance according to the application requirements
#############################################################################
##
## INSTALL FROM YUM by Developer
## yum -y install somethinghere...
##
#############################################################################

### Supervisor only works with Python2.7
easy_install pip
pip install supervisor
### Project needs python 3 ###
yum -y install mysql-devel python-devel python36-devel python36-setuptools nginx
rm /etc/nginx/nginx.conf
mv /home/ec2-user/nginx.conf /etc/nginx/nginx.conf
mv /home/ec2-user/.htpasswd /etc/nginx/.htpasswd
mkdir /etc/nginx/sites-available/
mkdir /etc/nginx/sites-enabled/
chown -R ec2-user:root /var/lib/nginx
chown -R ec2-user:root /var/log/nginx
service nginx start
service nginx restart
mv /home/ec2-user/scripts/env/dev_nginx.conf /etc/nginx/sites-available/dev_nginx.conf
ln -s /etc/nginx/sites-available/dev_nginx.conf /etc/nginx/sites-enabled/dev_nginx.conf

easy_install-3.6 pip
pip3 install virtualenv --upgrade
export PATH=/usr/local/bin:$PATH
cd /home/ec2-user
virtualenv -p python3 --no-site-packages environment
source environment/bin/activate
pip3 install -r requirements.txt --no-cache-dir
service awslogs restart