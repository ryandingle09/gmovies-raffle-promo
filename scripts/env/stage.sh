#####################################
## 
##    _____ _______       _____ ______ 
##   / ____|__   __|/\   / ____|  ____|
##  | (___    | |  /  \ | |  __| |__   
##   \___ \   | | / /\ \| | |_ |  __|  
##   ____) |  | |/ ____ \ |__| | |____ 
##  |_____/   |_/_/    \_\_____|______|
## 
#####################################
## Prepare your Canary Server here.
#####################################

#############################################
## CODECOMMIT REPOSITORY NAME - *REQUIRED* ##
#############################################
## Used for log and metrics tagging.       ##
#############################################
PROJECT_NAME=CICD-AICA-Chatbot-NLP


#############################################################################
## OS / APPLICATION SPECIFIC COMMANDS 
#############################################################################
## Prepare the instance according to the application requirements
#############################################################################
##
## INSTALL FROM YUM by Developer
## yum -y install somethinghere...
##
#############################################################################

easy_install pip
pip install virtualenv
cd /home/ec2-user
virtualenv environment
source environment/bin/activate
pip install -r requirements.txt
