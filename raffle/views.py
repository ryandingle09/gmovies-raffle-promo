from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from datetime import datetime, timedelta
import json
import ast
import MySQLdb

from django.core.cache import cache
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.db.models import Q
from django.db.models import Sum

from .models import Customer, Transaction
from .sureseats import create_sureseats_request
from .sms_spiels import UNREGISTERED, REGISTERED_RESERVED, REGISTERED_PAID
from .sms_spiels import REGISTERED_CANCELLED, DAILY_TOTAL
from lxml import etree
import requests
import time

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
PREFIX = ['0905', '0906', '0915', '0916', '0917', '0926', '0927', '0935', '0936', '0937',
        '0945', '0955', '0956', '0965', '0966', '0975', '0977', '0994', '0995', '0996', '0997']

def point_calculator(mulitplier, transaction_seat_count):
    ### Calculates total entry points
    # Multiplier Legend:
    # AA - Advanced Booking
    # A  - Seat Entry
    # B  - Globe Subscriber
    # C  - GCash App payment type
    # D  - Gcash payment type
    ###
    mulitplier = ast.literal_eval(mulitplier)
    default_seat_points = 1
    total_entries = 0
    additional_points = default_seat_points

    ###Additional Entry Points###
    #advance bookings
    if 'AA' in mulitplier:
        additional_points += 2

    #globe subscribers
    if 'B' in mulitplier:
        additional_points += 1
    
    #paid using gcash or used gcash app
    if 'C' in mulitplier or 'D' in mulitplier:
        additional_points += 1
        
    total_entries = transaction_seat_count * additional_points
    
    return total_entries


def get_multiplier(mobile, transaction_show_date, transaction_state, 
            transaction_payment_type):
    ### Calculates total entry points
    # Multiplier Legend:
    # AA - Advanced Booking
    # A  - Seat Entry
    # B  - Globe Subscriber
    # C  - GCash App payment type
    # D  - Gcash payment type
    ###

    multiplier = ['A']
    total_entries = 0
    show_date = datetime(2019, 4, 24).date()
    
    #Transaction entry validation
    if transaction_state == 'TX_DONE' and transaction_payment_type in ['globe-promo']:
        multiplier = []

    ###Additional Entry Points###
    else:  
        #advance bookings
        if transaction_show_date < show_date:
            multiplier.append('AA')

        #globe subscribers
        if mobile[0:4] in PREFIX:
            multiplier.append('B')
        
        #paid using gcash or used gcash app
        if transaction_payment_type in ['gcash', 'g-cash-app']:
            if transaction_payment_type == 'gcash':
                multiplier.append('D')
            else:
                multiplier.append('C')
    return total_entries, multiplier


@csrf_exempt
def raffle_entry(request):
    if request.method == "POST":
        start = time. time()
        raw_data = json.loads(request.body)
        data = raw_data['data'][0]['transaction']
        
        mobile = data['user_info'][0]['mobile']
        payment_type = data['payment_info'][0]['payment_type']

        transaction_id = data['primary_info'][0]['transaction_id']
        platform = data['primary_info'][0]['platform']
        payment_reference_code = data['primary_info'][0]['payment_reference']
        reservation_reference_code = data['primary_info'][0]['reservation_reference']
        transaction_date = datetime.strptime(data['primary_info'][0]['transaction_date'], '%Y-%m-%d %H:%M:%S')
        state = data['primary_info'][0]['state']

        ticket_code = data['ticket_info'][0]['ticket_code']
        show_date = datetime.strptime(data['ticket_info'][0]['ticket_date'], '%Y-%m-%d').date()
        theater_name = data['ticket_info'][0]['cinema_partner']
        theater_name = str(theater_name).replace("'","")
        theaterorg_name = data['ticket_info'][0]['theater_organization'].lower()

        seat_count = len(data['reservation_info'])
        seat_ids = []
        for seat in data['reservation_info']:
            seat['cinema'] = str(seat['cinema']).replace("'","")

            if seat['seat_id']:
                seat_ids.append(seat['seat_id'])
            else:
                seat_ids = "['FS']"
                break

        if payment_type in ['cash']:
            state = 'TX_RESERVED'

        total_entry, multiplier = get_multiplier(mobile, show_date, state, payment_type)

        customer_dict ={"mobile": mobile}

        customer, created = Customer.objects.get_or_create(defaults=customer_dict, mobile=mobile)
        
        first_name = customer.first_name
        last_name = customer.last_name
        
        customer_dict['id'] = str(customer.id)
        customer_dict['first_name'] = first_name
        customer_dict['last_name'] = last_name
        customer_dict['email'] = customer.email
        customer_dict['postal_addr'] = customer.postal_addr
        customer_dict['is_updated'] = str(customer.is_updated)
        customer_dict['created_at'] = str(customer.created_at)
        customer_dict['updated_at'] = str(customer.updated_at)
        
        customer_dict = {customer.id: customer_dict}

        if created:
            cached_customer = cache.get("Customers")
            if not cached_customer:
                print("empty customer cache")
                cached_customer = {}
            else:
                cached_customer = json.loads(cached_customer)

            cached_customer.update(customer_dict)
            cache.set("Customers", json.dumps(cached_customer), timeout=None)
    
        transaction_dict = {"customer": customer, "gae_transaction_id": transaction_id, 
                        "theater_name": theater_name, "transaction_date": transaction_date, 
                        "show_date": show_date, "ticket_code": ticket_code, 
                        "payment_type": payment_type,
                        "state": state, "platform": platform, 
                        "payment_reference_code": payment_reference_code, 
                        "reservation_reference_code": reservation_reference_code,
                        "data": str(data), "theaterorg_name": theaterorg_name,
                        "transaction_entry": total_entry, "multiplier": multiplier,
                        "seat_ids": str(seat_ids), "seat_count": seat_count}

        transaction, trans_created = Transaction.objects.get_or_create(defaults=transaction_dict, gae_transaction_id=transaction_id)#**transaction_dict)
        transaction_dict['customer'] = str(customer.id)
        transaction_dict['transaction_date'] = str(transaction_date)
        transaction_dict['show_date'] = str(show_date)
        transaction_dict['customer_first_name'] = first_name
        transaction_dict['customer_last_name'] = last_name
        transaction_dict['created_at'] = str(transaction.created_at)
        transaction_dict['updated_at'] = str(transaction.updated_at)
        transaction_dict['id'] = str(transaction.id)
        transaction_dict = {transaction_id: transaction_dict}
        cached_transaction = cache.get("Transactions")
        if trans_created:
            if not cached_transaction:
                print("empty transaction cache")
                cached_transaction = {}
            else:
                cached_transaction = json.loads(cached_transaction)
            
            cached_transaction.update(transaction_dict)
            
        else:
            transaction.state = state
            if customer.is_updated and state not in ['TX_DONE', 'TX_RESERVED']:
                send_sms(mobile, 'registered-cancelled')
                transaction.transaction_entry = 0
            transaction.save()

            date_yesterday = datetime.now() - timedelta(days=1)
            if transaction.transaction_date.date() < date_yesterday.date():
                ### call endpoint from Gia
                print('call endpoint to remove raffle tix')

            cached_transaction = json.loads(cached_transaction)
            cached_data = cached_transaction[transaction.gae_transaction_id]
            cached_data['state'] = state
            cached_data['transaction_entry'] = transaction.transaction_entry
            cached_data['updated_at'] = str(transaction.updated_at)

        cache.set("Transactions", json.dumps(cached_transaction), timeout=None)
        
        if not customer.is_updated and transaction.state in ['TX_DONE', 'TX_RESERVED']:
            send_sms(mobile, 'unregistered')
        elif customer.is_updated and transaction.state == 'TX_RESERVED':
            send_sms(mobile, 'registered-reserved')
        elif customer.is_updated and transaction.state == 'TX_DONE':
            send_sms(mobile, 'registered-paid')


        end = time. time()
        print(end - start)
    return HttpResponse(200)

def sureseats_requests(code, is_staging):
    endpoint = 'http://api.sureseats.com/globe.asp'
    staging = 'http://api.sureseats.com/globe_test.asp'
    action = 'STATUS'
    source_id = '9'
    is_staging = True

    if is_staging:
        endpoint = staging
    
    status_payload = {'src': source_id, 'action': action, 'claim_code': str(code)}
    print("payload: %s" % str(status_payload))
    r = create_sureseats_request(endpoint, status_payload)

    print("######################### START ############################")
    print(r.url)
    print("########################## END #############################")

    if r.status_code != 200:
        print("ERROR! cash_reservation_verification, cannot perform status retrieval, there was an error calling SureSeats...")
        return '', 'Status code not 200'
    
    print("update_status_by_qrcode, got response: %s..." % str(r.text))

    xml = etree.fromstring(str(r.text).encode('ISO-8859-1'))
    
    try:
        xml_status = str(xml.xpath('//TransStatus/Status/text()')[0]).lower()
    except IndexError:
        xml_status = ''
    try:
        error_message = str(xml.xpath('//TransStatus/Error/text()')[0])
    except IndexError:
        error_message = ''

    return xml_status, error_message

def transaction_verification(requests):
    start = time. time()
    date_yesterday = (datetime.now() - timedelta(days=1)).date()
    cached_transaction = json.loads(cache.get("Transactions"))

    ayala_transactions = Transaction.objects.filter(Q(theaterorg_name__contains='ayala'),
                                                    Q(show_date__gte=date_yesterday),
                                                    Q(state='TX_DONE') | Q(state='TX_RESERVED'))

    for transaction in ayala_transactions:
        code = transaction.ticket_code
        xml_status, error_message = sureseats_requests(code, True)
        state = transaction.state
        now = transaction.updated_at
        payment_type = transaction.payment_type
        
        print("payment_type: ", payment_type)
        
        if xml_status and not error_message:
            print("xml_status: ", xml_status)
            if payment_type == 'cash' and xml_status == 'claimed':
                state = 'TX_DONE'
                transaction.state = state
                transaction.save()

            if xml_status == 'forfeited':
                state = 'TX_FORFEITED'
                transaction.state = state
                transaction.transaction_entry = 0
                transaction.save()
                if transaction.transaction_date.date() < date_yesterday and payment_type != 'cash':
                    ### call Gia's endpoint
                    print('call endpoint to remove raffle tix')

            elif xml_status == 'cancelled':
                state = 'TX_REFUNDED'
                transaction.state = state
                transaction.transaction_entry = 0
                transaction.save()
                if transaction.transaction_date.date() < date_yesterday and payment_type != 'cash':
                    ### call Gia's endpoint
                    print('call endpoint to remove raffle tix')
            

            cached_data = cached_transaction[transaction.gae_transaction_id]
            cached_data['transaction_entry'] = transaction.transaction_entry
            cached_data['state'] = state
            cached_data['updated_at'] = str(transaction.updated_at)
            
    cache.set("Transactions", json.dumps(cached_transaction), timeout=None)
    end = time. time()
    print(end - start)
    return HttpResponse(len(ayala_transactions))

def send_sms(mobile, sms_type, total_raffle=None):
    endpoint = 'https://core.gmovies.ph/api/gmovies/sms/send'
    headers = {
        "LBAPIToken" : "Mc78beJQcwdEbWWcBqkF4yT7gQ38eWVH2Ws5VeUJT6jEpuGYmYDj4jYAwCT3mjpRr6QW2b6S2VZsS7QzuMcJPhzF",
        "Accept" : "application/json"
    }
    
    if sms_type == 'unregistered':
        message = UNREGISTERED
    elif sms_type == 'registered-reserved':
        message = REGISTERED_RESERVED
    elif sms_type == 'registered-paid':
        message = REGISTERED_PAID
    elif sms_type == 'registered-cancelled':
        message = REGISTERED_CANCELLED
    elif sms_type == 'daily_total':
        message = DAILY_TOTAL.format(str(total_raffle))
    payload = {
        'message': message,
        'mobile': mobile,
        'send_by': 'gae',
    }
    r = requests.post(endpoint, data=payload, headers=headers)
    d = r.json()
    print('sms status: ', d)
    return d

def update_transaction_entry(transactions):
    customer_list = []
    cached_transaction = json.loads(cache.get("Transactions"))

    for transaction in transactions:
        if transaction.customer not in customer_list:
            customer_list.append(transaction.customer)
        total_entries = point_calculator(transaction.multiplier, transaction.seat_count)
        transaction.transaction_entry = total_entries
        transaction.save()

        cached_data = cached_transaction[transaction.gae_transaction_id]
        cached_data['transaction_entry'] = total_entries
        cached_data['updated_at'] = str(transaction.updated_at)
    cache.set("Transactions", json.dumps(cached_transaction), timeout=None)
    return customer_list

def daily_transaction_entry_sms(request):
    date_yesterday = (datetime.now() - timedelta(days=1)).date()
    cached_transaction = json.loads(cache.get("Transactions"))

    ### all customers who updated yesterday
    customers_updated_yesterday = Customer.objects.filter(Q(updated_at__contains=date_yesterday), 
                                                    Q(is_updated=True))

    for customer in customers_updated_yesterday:
        past_transactions = Transaction.objects.filter(Q(state='TX_DONE'), 
                                            Q(customer=customer))
        customer_list = update_transaction_entry(past_transactions)
        total_raffle = Transaction.objects.filter(customer=customer).aggregate(Sum('transaction_entry'))['transaction_entry__sum']
        send_sms(customer.mobile, 'daily_total', total_raffle=total_raffle)

    ### all transactions made yesterday for all customers that is updated before yesterday
    yesterday_transactions = Transaction.objects.filter(Q(transaction_date__contains=date_yesterday),
                                Q(state='TX_DONE'), customer__is_updated=True).exclude(customer__in=customer_list)
    customer_list = update_transaction_entry(yesterday_transactions)

    for customer in customer_list:
        total_raffle = Transaction.objects.filter(customer=customer, state='TX_DONE').aggregate(Sum('transaction_entry'))['transaction_entry__sum']
        send_sms(customer.mobile, 'daily_total', total_raffle=total_raffle)


    return HttpResponse(200)

@csrf_exempt
def test_cash_code(request):
    raw_data = json.loads(request.body)
    print(raw_data)
    data = raw_data['data']['code']
    xml_status, error_message = sureseats_requests(data, True)


    return HttpResponse(xml_status)