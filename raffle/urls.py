from django.contrib import admin
from django.urls import path, include
from . import views
urlpatterns = [
    path('raffle-entry', views.raffle_entry, name='raffle_entry'),
    path('transaction-verification', views.transaction_verification, name='tx_verification'),
    path('daily-sms', views.daily_transaction_entry_sms, name='daily_sms'),
    path('cash-code', views.test_cash_code, name='test_cash_code'),
]
