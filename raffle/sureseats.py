from __future__ import absolute_import, unicode_literals
from Crypto.Cipher import AES
import requests
from lxml import html
import binascii

BS = 16
DUMMY = 'http://api.sureseats.com/globe.asp'
SURESEATS_AES_KEY = '0123456789abcdef'
SURESEATS_AES_IV = 'fedcba9876543210'

import base64
import logging

log = logging.getLogger(__name__)

def pkcs5_padding( s):
    padding_length = (BS - len(s) % BS)
    result = s + padding_length * chr(padding_length)

    return result

def sureseats_encrypt_params(string):
    aes = AES.new(SURESEATS_AES_KEY, AES.MODE_CBC, SURESEATS_AES_IV)
    enc = pkcs5_padding(string + '\0')
    enc = aes.encrypt(enc)
    # enc = enc.encode("hex")
    enc = binascii.hexlify(enc)

    return enc

def sureseats_decrypt_params(enc):
    enc = base64.b64decode(enc)
    iv = enc[:AES.block_size]
    cipher = AES.new(key, AES.MODE_CBC, iv)

    return _unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

def _unpad(s):
    return s[:-ord(s[len(s) - 1:])]

def create_sureseats_request(endpoint, params):
    pass1_params = dict(**params)
    del pass1_params['action']
    
    pass1 = requests.Request('POST', url=endpoint, data=pass1_params).prepare()
    param_string = pass1.body

    print("param_string_body: %s" % (param_string))

    encrypted_value = sureseats_encrypt_params(param_string)

    print("encrypted_value: %s" % (encrypted_value))

    req_params = {'action': params['action'], 'en': encrypted_value}

    print("req_params: %s" % (req_params))

    req = requests.get(url=endpoint, params=req_params)

    return req

# params = {
#     "action": "LOGIN",
#     "un":"gmovies_promo",
#     "pw":"gmovies123",
# }

#data = a.sureseats_encrypt_params(str(params))
#data = create_sureseats_request(DUMMY, params)
#print data.status_code