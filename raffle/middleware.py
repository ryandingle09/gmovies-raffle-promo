from django.conf import settings
from django.utils.deprecation import MiddlewareMixin
from django.http import HttpResponse
import os
import environ

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

env = environ.Env(DEBUG=(bool, False), )  # set default values and casting
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))

import json
DEVICE_ID = 'HTTP_X_GMOVIES_DEVICEID'

class GmoviesAPIMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if request.method == 'POST':
            header = request.META.get(DEVICE_ID)
            hostname = request.META.get('REMOTE_HOST')

            if not hostname:
                hostname = request.META.get('REMOTE_ADDR')

            if str(hostname) in str(env('ALLOWED_HOST')):
                return None

            # print(request.META.keys())    
            print('header: ', header)
            print('hostname: ', hostname)
            
            if not header:
                print('No header')
                return HttpResponse(json.dumps({"error": "Incorrect header"}), content_type="application/json", status=400)

            elif hostname not in str(env(ALLOWED_CONNECTION)):
                return HttpResponse(json.dumps({"error": "Access Denied"}), content_type="application/json", status=400)

            else:
                return None

        return None

