# Generated by Django 2.1.7 on 2019-03-07 10:50

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20190305_0320'),
    ]

    operations = [
        migrations.AlterField(
            model_name='passwordresets',
            name='created_at',
            field=models.DateTimeField(verbose_name=datetime.datetime(2019, 3, 7, 10, 50, 25, 692934, tzinfo=utc)),
        ),
    ]
