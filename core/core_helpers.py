from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.core import serializers
from django.utils import timezone
from group.models import User as UserGroups
from permission.models import UL as Permissions
from django.core.mail import send_mail

#generate token
class GenerateToken(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (six.text_type(user.pk) 
            + six.text_type(timestamp)
            + six.text_type(timezone.now())
            + six.text_type(user.email)
        )

#method for sending/resending email activation
class SendEmail:
	def  __call__(self, request, user, data, subject, template):
	    current_site = get_current_site(request)
	    subject = subject
	    message = render_to_string(template, {
	        'user': user,
	        'data': data,
	        'domain': current_site.domain,
	        'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
	        'token': generate_token.make_token(user),
	    })
	    send_mail(subject, message, 'support@gmovies.ph', [user.email], fail_silently=False)
		#user.email_user(subject, message)
        #send_mail(subject, message, user.email, fail_silently=False)

#return json seriazlize data
class Json:
	def __call__(self, model):
		data = serializers.serialize("json", model)
		return data

#return current user loggedin permissions
class GetPermissions:
	def __call__(self, l_user_id):
		u_group         = UserGroups.objects.filter(user_id=l_user_id)
		u_permissions   = str(u_group[0].group.permissions).replace("'","").replace("[","").replace("]","")
		u_permissions   = u_permissions.split(',')
		
		ar_p = []
		for i in u_permissions:
			ar_p.append(int(i))
			
		o_permisssion   = Permissions.objects.filter(pk__in=ar_p)

		return o_permisssion.values()

#alias call import
generate_token = GenerateToken()
send_email = SendEmail()
to_json = Json()
get_permissions = GetPermissions()



