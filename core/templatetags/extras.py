from django import template
from core.core_helpers import get_permissions
import json

def cutList(value, arg):
    val = str(value)
    val = val.replace("'", '')
    val = val.replace("[", '')
    val = val.replace("]", '')

    return val

def plus(value):
    val = int(value) + 1

    return val

def multiply(value, num):
    val = int(value) * num

    return val

def length(value):
    val = len(value)

    return val

def toPerPage(num, num2):
    lis = []

    for a in range(num+1):
        if (a%10) == 0:
            lis.append(a)
    
    lis.remove(0)

    return lis

def getAttr(ar, type):
    for i,a in ar.items():
        return i if type == 'key' else a

def toStrArr(ar):
    return ', '.join(map(str, ar))

def toArr(string):
    return string.split()

def toInt(string):
    return string.replace("'", "")

def toStr(string):
    return str(string)

def toUpper(string):
    return str(string).upper()

def toLower(string):
    return str(string).lower()

def getPermissions(user_id):
    return get_permissions(user_id)

def removeColon(string):
    return string.replace(':','')

def retvalues(ar):
    newar = []

    for a in ar:
        if a['seat_id'] == '':
            newar.append('FS')
        else:
            newar.append(a['seat_id'])

    return newar  

def firstUpper(string):
    return string[0].upper()

register = template.Library()
register.filter('cutList', cutList)
register.filter('plus', plus)
register.filter('multiply', multiply)
register.filter('len', length)
register.filter('toPerPage', toPerPage)
register.filter('getAttr', getAttr)
register.filter('toStrArr', toStrArr)
register.filter('toStr', toStr)
register.filter('toArr', toArr)
register.filter('toInt', toInt)
register.filter('toLower', toLower)
register.filter('getPermissions', getPermissions)
register.filter('removeColon', removeColon)
register.filter('retvalues', retvalues)
register.filter('firstUpper', firstUpper)