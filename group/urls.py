from django.urls import path
from . import views

app_name = 'group'
urlpatterns = [
    path('list', views.List.as_view(), name='group-list'),
    path('add', views.Add.as_view(), name='add'),
    path('edit/<int:group_id>', views.Edit.as_view(), name='edit'),
    path('delete/<int:group_id>', views.Delete.as_view(), name='delete'),
]