from django.db import models
from django.contrib.auth.models import User

class UL(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.TextField()
    description = models.TextField(null=True)
    permissions = models.TextField(null=True)

class User(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(UL, on_delete=models.CASCADE, null=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
