from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views import generic, View
from django.utils import timezone
from .models import UL as Group
from permission.models import UL as Permission

class List(LoginRequiredMixin,View):
    template_name   = 'group/list.html'
    permissions     = Permission.objects.all()

    def get(self, request, *args, **kwargs):
        offset      = int(request.GET['offset']) if 'offset' in request.GET else 0
        limit       = int(request.GET['limit']) if 'limit' in request.GET else 10
        page        = int(request.GET['page']) if 'page' in request.GET else 1
        search      = str(request.GET['search']) if 'search' in request.GET else None

        items       = Group.objects.all()
        paginator   = Paginator(items, limit)
        data_list   = paginator.get_page(page)

        data = {
            'items': data_list,
            'all_count': paginator.count,
            'current_count': len(data_list),
            'offset': offset,
            'limit': limit,
            'page': page,
            'permissions': self.permissions
        }

        if search is not None:
            items = Group.objects.all()

            if search != '':
                items = Group.objects.filter(name__contains=search) | Group.objects.filter(description__contains=search)

            paginator   = Paginator(items, limit)
            data_list   = paginator.get_page(page)

            data = {
                'items': data_list,
                'all_count': paginator.count,
                'current_count': len(data_list),
                'offset': offset,
                'limit': limit,
                'page': page,
            }

        return render(request, self.template_name, data)

class Add(LoginRequiredMixin,View):
    errors          = []
    template_name   = 'group/add.html'
    message         = ''
    permissions     = Permission.objects.all()

    def get(self, request):
        data = {
            'permissions': self.permissions, 
            'message': self.message,
            'errors': self.errors
        }

        return render(request, self.template_name, data)

    def post(self, request):
        g_name        = request.POST['name'] if request.POST['name'] != '' else None
        g_description = request.POST['description'] if request.POST['description'] != '' else None
        g_permission  = request.POST.getlist('permission[]')

        self.errors = []

        if g_name is None:
            self.errors.append('Name Field is Required.')

        if len(g_permission) == 0:
            self.errors.append('Permission Field is Required.')
        
        if len(self.errors) == 0:
            self.message = 'Successfully Added.'
            Group(name=g_name, description=g_description, permissions=g_permission).save()

            data = {
                'message': self.message,
                'errors': self.errors
            }
            
            return JsonResponse(data)

        self.message = 'Fields are required.'

        data = {
            'message': self.message,
            'errors': self.errors
        }
        
        return JsonResponse(data)

class Edit(LoginRequiredMixin,View):
    template_name   = 'group/edit.html'
    permissions     = Permission.objects.all()
    message         = ''
    errors          = []

    def get(self, request, group_id):
        g       = Group.objects.filter(id=group_id)
        data    = {
            'items': g,
            'permissions': self.permissions,
            'errors': self.errors,
            'message': self.message
        }

        return render(request, self.template_name, data)
    
    def post(self, request, group_id):
        g_name        = request.POST['name'] if request.POST['name'] != '' else None
        g_description = request.POST['description'] if request.POST['description'] != '' else None
        g_permission  = request.POST.getlist('permission[]')

        g = Group.objects.filter(id=group_id)

        self.errors = []

        if g_name is None:
            self.errors.append('Name Field is Required.')

        if len(g_permission) == 0:
            self.errors.append('Permission Field is Required.')
        
        if len(self.errors) == 0:
            self.message = 'Successfully Added.'
            g.update(name=g_name, description=g_description, permissions=g_permission)

            data    = {
                'errors': self.errors,
                'message': self.message
            }

            return JsonResponse(data)

        self.message = 'Fields are required.'

        data    = {
            'errors': self.errors,
            'message': self.message
        }

        return JsonResponse(data)

class Delete(LoginRequiredMixin,View):
    def get(self, request, group_id):
        Group.objects.filter(id=group_id).delete()

        return HttpResponseRedirect('/group/list?status=deleted')