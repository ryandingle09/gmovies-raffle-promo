from django import forms

class GroupForm(forms.Form):
    name = forms.TextField( required=True)
    description = forms.TextField()
    permission = forms.CharField(max_length=100, required=True)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(username=username, password=password)

            if not user or not user.is_active:
                raise forms.ValidationError("Sorry, Invalid username or password. Please try again.")

        return self.cleaned_data