from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator
from django.views import generic, View
from django.utils import timezone
from django.core.cache import cache
from raffle.models import Transaction, Customer
import json

class List(LoginRequiredMixin,View):
    template_name = 'transaction/list.html'
    theater_orgs  = ['AYALA MALLS', 'ROBINSONS MALLS', 'GREENHILLS MALLS', 'MEGAWORLDS MALLS', 'CITY MALLS', 'CINEMA76 MALLS']
    search_fields = [
        {'ticket_code':'Ticket Code'},
        {'name':'Customer Name'}, 
        {'email':'Customer Email'}, 
        {'mobile':'Customer Mobile'}
    ]

    def get(self, request):
        offset          = int(request.GET['offset']) if 'offset' in request.GET else 0
        limit           = int(request.GET['limit']) if 'limit' in request.GET else 10
        page            = int(request.GET['page']) if 'page' in request.GET else 1
        search          = str(request.GET['search']) if 'search' in request.GET else None
        field           = str(request.GET['field']) if 'field' in request.GET else None
        theater_org     = str(request.GET['theater_org']) if 'theater_org' in request.GET else None
        start_date      = str(request.GET['start_date']) if 'start_date' in request.GET else None
        end_date        = str(request.GET['end_date']) if 'end_date' in request.GET else None

        transaction_cache = cache.get('Transactions')

        transaction     = str(transaction_cache).replace('"{', '{').replace('}"', '}').replace('"[', '[').replace(']"', ']').replace("'", '"').replace('None', 'null').replace("/","")
        all_count       = len(list(json.loads(transaction).values())) if transaction_cache else 0
        current_count   = len(list(json.loads(transaction).values())[offset:(offset+limit)]) if transaction_cache else 0
        pages_count     = int(all_count / limit) if transaction_cache else 0

        pages           = 0
        prev            = page-2 if page-2 > 0 else 0
        current         = page-1 if page-1 > 0 else 0

        if current == 0:
            pages = [prev, page]

        elif page == pages_count:
            pages = [prev, page-1]
            
        else:
            pages = [prev, current, page]

        data = {
            'transactions': reversed(list(json.loads(transaction).values())[offset:(offset+limit)]) if transaction_cache else [],
            'all_count': all_count,
            'current_count': current_count,
            'offset': offset,
            'limit': limit,
            'page': page,
            'pages': pages,
            'theater_orgs': self.theater_orgs,
            'fields': self.search_fields,
            'onsearch': False
        }

        if search is not None:
            tr = Transaction.objects.all()

            if search != '':
                tr = Transaction.objects.filter(theater_name__contains=search) | Transaction.objects.filter(theaterorg_name__contains=search) | Transaction.objects.filter(transaction_date__contains=search) | Transaction.objects.filter(show_date__contains=search) | Transaction.objects.filter(ticket_code__contains=search) | Transaction.objects.filter(payment_type__contains=search) | Transaction.objects.filter(state__contains=search) | Transaction.objects.filter(platform__contains=search) | Transaction.objects.filter(payment_reference_code__contains=search) | Transaction.objects.filter(reservation_reference_code__contains=search) | Transaction.objects.filter(transaction_entry__contains=search) | Transaction.objects.filter(multiplier__contains=search) | Transaction.objects.filter(customer__first_name__contains=search) | Transaction.objects.filter(customer__last_name__contains=search) | Transaction.objects.filter(customer__email__contains=search) | Transaction.objects.filter(customer__mobile__contains=search)

            if theater_org != '':
                tr = tr.filter(theaterorg_name=theater_org)

            if start_date != '' and end_date != '':
                tr = tr.filter(created_at__range=(start_date, end_date))

            tr = tr.order_by('-id')

            paginator   = Paginator(tr, limit)
            tr_list     = paginator.get_page(page)

            data = {
                'transactions': tr_list,
                'all_count': paginator.count,
                'current_count': len(tr_list),
                'offset': offset,
                'limit': limit,
                'page': page,
                'theater_orgs': self.theater_orgs,
                'fields': self.search_fields,
                'onsearch': True,
                'search': search,
                'chosen_org': theater_org,
                'start_date': start_date,
                'end_date': end_date
            }

        return render(request, self.template_name, data)