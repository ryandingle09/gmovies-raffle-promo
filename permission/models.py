from django.db import models

class UL(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.TextField()
    slug = models.TextField()
