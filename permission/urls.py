from django.urls import path
from . import views

app_name = 'permission'
urlpatterns = [
    path('list', views.List.as_view(), name='permission-list'),
    path('add', views.Add.as_view(), name='add'),
    path('edit/<int:permission_id>', views.Edit.as_view(), name='edit'),
    path('delete/<int:permission_id>', views.Delete.as_view(), name='delete'),
]