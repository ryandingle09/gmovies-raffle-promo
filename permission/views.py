from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views import generic, View
from django.utils import timezone
from .models import UL as Permission

class List(LoginRequiredMixin,View):
    template_name = 'permission/list.html'

    def get(self, request, *args, **kwargs):
        offset      = int(request.GET['offset']) if 'offset' in request.GET else 0
        limit       = int(request.GET['limit']) if 'limit' in request.GET else 10
        page        = int(request.GET['page']) if 'page' in request.GET else 1
        search      = str(request.GET['search']) if 'search' in request.GET else None

        items       = Permission.objects.all()
        paginator   = Paginator(items, limit)
        data_list   = paginator.get_page(page)

        data = {
                'items': data_list,
                'all_count': paginator.count,
                'current_count': len(data_list),
                'offset': offset,
                'limit': limit,
                'page': page
            }
        
        if search is not None:
            items = Permission.objects.all()

            if search != '':
                items = Permission.objects.filter(name__contains=search) | Permission.objects.filter(slug__contains=search)

            paginator   = Paginator(items, limit)
            data_list   = paginator.get_page(page)

            data = {
                'items': data_list,
                'all_count': paginator.count,
                'current_count': len(data_list),
                'offset': offset,
                'limit': limit,
                'page': page,
            }

        return render(request, self.template_name, data)

class Add(LoginRequiredMixin,View):
    errors          = []
    template_name   = 'permission/add.html'
    message         = ''

    def get(self, request):
        data = { 
            'message': self.message,
            'errors': self.errors
        }

        return render(request, self.template_name, data)

    def post(self, request):
        p_name = request.POST['name'] if request.POST['name'] != '' else None
        p_slug = request.POST['slug'] if request.POST['slug'] != '' else None

        self.errors = []

        if p_name is None:
            self.errors.append('Name Field is Required.')

        if p_slug is None:
            self.errors.append('Slug Field is Required.')

        if p_slug is not None:
            p = Permission.objects.filter(slug=p_slug)

            if len(p) != 0:
                self.errors.append('Slug Field must be unique.')
        
        if len(self.errors) == 0:
            self.message = 'Successfully Added.'
            Permission(name=p_name, slug=p_slug).save()

            data = {
                'message': self.message,
                'errors': self.errors
            }

            return JsonResponse(data)

        self.message = 'Fields are required.'

        data = {
            'message': self.message,
            'errors': self.errors
        }

        return JsonResponse(data)

class Edit(LoginRequiredMixin,View):
    template_name   = 'permission/edit.html'
    message         = ''
    errors          = []

    def get(self, request, permission_id):
        g       = Permission.objects.filter(id=permission_id)
        data    = {
            'items': g,
            'errors': self.errors,
            'message': self.message
        }

        return render(request, self.template_name, data)
    
    def post(self, request, permission_id):
        p_name = request.POST['name'] if request.POST['name'] != '' else None
        p_slug = request.POST['slug'] if request.POST['slug'] != '' else None

        g = Permission.objects.filter(id=permission_id)

        self.errors = []

        if p_name is None:
            self.errors.append('Name Field is Required.')

        if p_slug is None:
            self.errors.append('Slug Field is Required.')

        if p_slug is not None:
            if g[0].slug != p_slug:
                p = Permission.objects.filter(slug=p_slug)

                if len(p) != 0:
                    self.errors.append('Slug Field must be unique.')
        
        if len(self.errors) == 0:
            self.message = 'Successfully Added.'
            g.update(name=p_name, slug=p_slug)

            data    = {
                'errors': self.errors,
                'message': self.message
            }

            return JsonResponse(data)

        self.message = 'Fields are required.'

        data    = {
            'errors': self.errors,
            'message': self.message
        }

        return JsonResponse(data)

class Delete(LoginRequiredMixin,View):
    def get(self, request, permission_id):
        Permission.objects.filter(id=permission_id).delete()

        return HttpResponseRedirect('/permission/list?status=deleted')